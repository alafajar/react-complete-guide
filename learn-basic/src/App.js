import React, { Component } from 'react';
import Person from './Person/Person'

import './App.css'

/**
 * props : simply and object giving us access to all the attributes, we pass to our own components
 * state : special property, whereas props are set and passed from outside like name
 *         state is managed from inside a component
 * important, this state property here is only available like this in components that extend components so in class-based React components
 */

/**
 * definition:
 * props  allow you to pass data from a parent (wrapping) component to a child (embedded) component.
 * state Whilst props allow you to pass data down the component tree (and hence trigger an UI update), state is used to change the component, well, state from within. Changes to state also trigger an UI update.
 */

 //REACT HOOKS
//  const app = props => {
//     const [ personState, setPersonState ] = useState({
//       persons : [
//               { name: 'max', age : 28},
//               { name: 'doro', age : 29},
//               { name: 'werku', age : 21},
              
//             ],
//       otherState : 'some other value',

//     }) 

//     console.log('personState', personState)
//     console.log('setpersonState', setPersonState)

//     const switchNameHandler = () => {
//       setPersonState({
//         persons : [
//           { name: 'zero', age : 28},
//           { name: 'one', age : 29},
//           { name: 'two', age : 21},
//         ]
//       })
//     }

//     return(
//       <div className="App">
//            <h1 className="App-title">Welcome to React</h1>
//            <p>This is really working</p>
//            <button onClick={switchNameHandler}>Switch Name</button>
//            <Person name={personState.persons[0].name} property={personState.persons[0].age}/>
//            <Person name={personState.persons[1].name} property={personState.persons[1].age}>My hobbies : running</Person>
//            <Person name={personState.persons[2].name} property={personState.persons[2].age}/>
//        </div>
//     )
//  }

//  export default app




//CLASS BASED COMPONENTS

class App extends Component {
   /**
    * traditional style
   * this.state ={
   *  a : 'a'
   * }
   */

  // es6 style
  state = {
    persons : [
      { id:'asd1',name: 'max', age : 28},
      { id:'asd2',name: 'doro', age : 29},
      { id:'asd3',name: 'werku', age : 21},

    ],
    counter : 1,
    showPerson : false
  }

  // switchNameHandler = (newName) => {
  //   // console.log('was clicked')
  //   // DON'T DO THIS: this.state.persons[0].name = 'Maximilian'
  //   this.setState({
  //     persons: [
  //       { id: 'ad1',name: newName, age : 28},
  //       { id: 'as2',name: 'doro', age : 29},
  //       { id: 'ss3',name: 'werku', age : 30},
  //     ]
  //   })
  // }
  nameChangedHandler = ( id, event ) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    };

    // const person = Object.assign({}, this.state.persons[personIndex]);

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState( {persons: persons} );
  }

  deleteHandler = (personIndex) => {
    // const { persons } = this.state.slice() // old way
    const { persons }  = this.state //new way modern javascript
    let z = [...persons]
      z.splice(personIndex, 1)
      this.setState({
        persons: z
      })
  }

  

  toggleHandler = () => {
    const { showPerson } = this.state
    const doesShow = showPerson
    this.setState({
      showPerson: !doesShow
    })
  }
  render() {
    const { persons } = this.state
    const style = {
      backgroundColor : 'white',
      font : 'inherit',
      border : '1px solid blue',
      cursor : 'pointer',
      padding: '8px',
    }

    let person = null;

    if(this.state.showPerson){
      person = (
        <div>
          {persons.map((psn, index) => {
            return <Person 
              click={this.deleteHandler.bind(this, index)}
              key      = {psn.id}
              changed  = {this.nameChangedHandler.bind(this, psn.id)}
              name     = {psn.name}
              property = {psn.age}/>
          })}
        </div>
      )
    }

    return (
      <div className="App">
          <h1 className="App-title">Welcome to React</h1>
          <p>This is really working</p>
          <button style={style} onClick={this.toggleHandler}>Toggle Person</button>
          {person}
      </div>
    );
  }
}

export default App;
