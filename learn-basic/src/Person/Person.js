import React from 'react'
import './Person.css'

/**
 * 
 * children refers to any element and this includes plaint text as we have it here between the opening and closing tag of our component
 */

const person = ( props ) => {
    return (
        <div className="Person">
            <p onClick={props.click}>I'm {props.name} and I am {props.property} years old!</p>
            <p>{props.children}</p>
            <input type="text" onChange={props.changed} value={props.name} />
        </div>
    )
};

export default person;